#include <iostream>;
#include <iomanip>;
//#include <sys/time.h>;
using namespace std;
int fibbonacciRecursive(int);
void testRecursive();
void testIterative();
//float testSpeed();
void fibbonacciArray(int);
int ArrayAddition(int[], int[]);

void main(){
	char choice;
	cout << "A. Recursive" << endl << "B. Iterative" << endl << "Select an Option: ";
	cin >> choice;
	tolower(choice);

	switch (choice)
	{
		case'a':
			testRecursive();
			break;
		case 'b':
			testIterative();
			break;
		case 'c':
			//testSpeed();
			break;
		case 'd':
			int N = 0;
			cout << "Enter 475 to show the largest Fibonacci number with less than 100 digits." << endl;
			cout << "Enter an integer: ";
			cin >> N;
			fibbonacciArray(N);
	}
}


int fibbonacciIterative(int n) {
	if (n == 0)
        return 0;

    if (n == 1)
        return 1;

	int F0 = 0;
	int F1 = 1;
	int sum = 0;

	for( int i = 0; i < (n-1); i++)
	{
		sum = F0 + F1;
		F0 = F1;
		F1 = sum;
	}
	return sum;
}

int fibbonacciRecursive(int n) {
    if (n == 0)
        return 0;

    if (n == 1)
        return 1;

    return fibbonacciRecursive(n-1)+fibbonacciRecursive(n-2);
}

void testRecursive()
{
	int choice = 0;
	cout << "Enter a value: ";
	cin >> choice;
	for( int i = 0; i <= choice; i++)
	{
		cout <<"F(" << i <<")" << "=" << fibbonacciRecursive(i) << endl;
	}

	cin.ignore();
	cin.ignore();
}

void testIterative()
{
	int choice = 0;
	cout << "Enter a value: ";
	cin >> choice;
	for( int i = 0; i <= choice; i++)
	{
		cout <<"F(" << i <<")" << "=" << fibbonacciIterative(i) << endl;
	}

	cin.ignore();
	cin.ignore();
}

/*float testSpeed()
{
	float recursiveStart = 0;
	float recursiveEnd = 0;
	float recursiveTime = 0;

	float iterativeStart = 0;
	float iterativeEnd = 0;
	float iterativeTime = 0;

	int n = 0;
	cout << "Enter an integer: ";
	cin >> n;

	recursiveStart = gethrtime();
	fibbonacciRecursive(n);
	recursiveEnd = gethrtime();
	recursiveTime = recursiveEnd - recursiveStart;

	iterativeStart = gethrtime();
	fibbonacciIterative(n);
	iterativeEnd = gethrtime();
	iterativeTime = iterativeEnd - iterativeStart;

	cout << "Recursive Speed = " << recursiveTime << endl << "Iterative Speed = " << iterativeTime;
}
*/



void fibbonacciArray(int N)
{
	int size = 99;
	int n = N;
	
	int fibArray0[99];
	int fibArray1[99];
	int fibArraySum[100];

	for( int i = 0; i < size; i++)
	{
		fibArray0[i] = 0;
		fibArray1[i] = 0;
	}
	for( int i = 0; i <= size; i++)
	{
		fibArraySum[i] = 0;
	}

	fibArray1[98] = 1;
	for ( int j = 0; j < n-1; j++) 
	{
		

		int r = 0;
		for (int i = size - 1; i >= 0; i--) 
		{			
			int t = fibArray0[i] + fibArray1[i] + r;
			fibArraySum[i + 1] = t % 10;
			r = t / 10;
        }
        fibArraySum[0] = r;
	

		for(int i = 0; i < size; i++)
		{
			fibArray0[i] = fibArray1[i];
		}

		for(int i = 0; i < size; i++)
		{
			fibArray1[i] = fibArraySum[i+1];
		}
		string counter ="";
	}
	//Move this for loop above into the above bracket to print all Fibonacci numbers instead of just the nth one.
	for(int i = 0; i <= size; i++)
		{
			cout << fibArraySum[i];
		}

	cin.ignore();
	cin.ignore();

};